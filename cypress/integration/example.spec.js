describe('Jenkins Test', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('gets the skip link', () => {
    cy.get('.skip-nav')
        .contains('Skip Navigation')
  })

  // it('validates the main nav', () => {
  //   cy.get('[data-cy="header"]')
  //     .within(() => {
  //       cy.get('#home')
  //         .should('have.attr', 'href', '#')
  //         .contains('wrong')
  //     })
  // })

  // it('validates the h1 text', () => {
  //   cy.get('[data-cy="main"]')
  //     .within(() => {
  //       cy.get('h1').contains('Cypress')
  //     })
  // })
})